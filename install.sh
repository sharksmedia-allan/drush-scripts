#!/bin/bash

# Create a 'bin' dir in home dir that is in PATH
mkdir -p ~/bin
echo  'PATH=$PATH:~/bin' >> ~/.bashrc

source ~/.bashrc

# Install composer with script that allow allow_url_fopen
./composer-install.sh

echo 'PATH=$HOME:~/.composer/vendor/bin:$PATH' >> ~/.bashrc
composer global require drush/drush:7.*

cp ./composer ~/bin
cp ./drush ~/bin
cp ./drupal-up ~/bin
