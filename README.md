# Drush / composer setup

Will install drush and composer and add paths to .bashrc

Install: 

    git clone https://bitbucket.org/mikkelsjensen/drush-scripts

    cd drush-scripts

    ./install.sh

This installs drush for drupal 7 and latest composer. 

If you want drush for drupal 8, you need to do this after running the `install.sh`: 

    composer global remove drush/drush

    composer global require drush/drush:8.*

You can now run composer and drush like this: 

    drush 
 
    composer

# Backup

Backup all files and SQL:

    cd /home/dennis/drupal-site 

    drush arb

This creates e.g. 

    /home/dennis/drush-backups/archive-dump/20170420161151/drupaltest.20170420_161151.tar.gz

# Update

    drupal-up

# Restore (if failure) - both files and SQL

Step one dir up, e.g. 

    cd /home/dennis

    drush arr /home/dennis/drush-backups/archive-dump/20170420161151/drupaltest.20170420_161151.tar.gz --overwrite

(Notice the overwrite param)

